# pyGP: Python Gaussian Processes

This is pyGP, a minimal toolkit for using Gaussian process tools.

## Getting started

* To install the required packages automatically with pip, type:

~~~
  $ pip install -r requirements.txt
~~~

* Building the pyGP package can be accomplished by executing the shell script
  `install` in the package root directory; see `install -h` for a list of options.

~~~
   $ ./install -h
   Accepted options:
       -p  Compile with Cython profiling information
       -c  Clean previous before installation
       -d  Create documentation
~~~

