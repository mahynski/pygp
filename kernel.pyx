import numpy as np
import cython

cimport cython
cimport libc.math
cimport numpy as np
from cpython cimport bool

class Kernel(object):
    """
    **All inheriting subclasses should define the kernel() and
    partial_derivatives() methods.** The partial_derivatives should always be
    returned in sorted order according to parameter name.
    """
    def __init__(self, **params):
        self._params = params

    def __call__(self, x1, x2, oo=False, **kwargs):
        """
        Invoke the subclass' kernel() method. Refer to this method for the specific
        Kernel you are working with.

        Parameters
        ----------
        x1 : ndarray
            First matrix of independent variable coordinates, provided as rows.
        x2 : ndarray
            Second matrix of independent variable coordinates, provided as rows.
        oo : bool
            Are x1 and x2 both the observations? (to compute K matrix)
        kwargs : dict
            Additional keyword arguments.
        """

        for input_values in [x1, x2]:
            if (not isinstance(input_values, np.ndarray)):
                raise Exception('input must be a numpy array')
            if (len(input_values.shape) != 2):
                raise Exception('must provide input data as N x D array of row vectors')

        try:
            return self.kernel(x1, x2, oo, **kwargs)
        except AttributeError:
            raise Exception('{} does not have a kernel() method defined'.format(self.__repr__))

    @property
    def parameters(self):
        return self._params

    def update_parameters(self, **params):
        """
        Update existing parameters. New parameters the Kernel was not initialized with will be ignored.

        Parameters
        ----------
        params : dict
            Dictionary of (key, value) pairs for Kernel parameters
        """

        for k in params.keys():
            if (k in self._params.keys()):
                self._params[k] = params[k]

    def kernel(self, x1, x2, oo, **kwargs):
        """
        Compute the kernel function for a set of inputs.
        This should be defined by the user for each Kernel instance.
        """

        raise Exception('kernel() method not implemented')

    def partial_derivatives(self, x1, x2, oo, **kwargs):
        """
        Compute the partial derivatives of the Kernel with respect to its parameters.
        All inheriting subclasses should define this partial_derivatives() method.
        **Results should be returned in a sorted order according to the parameter names.**

        Notes
        -----
        Inputs are expected to be numpy arrays, but should be explicitly converted to
        np.float32_t in any user defined subclass for speed and to be generally consistent
        with other Kernels.
        """

        raise Exception('partial_derivatives() method not implemented')

class Matern(Kernel):
    """
    Matern kernel function.
    """

    def __init__(self, **params):
        super(Matern, self).__init__(**params)

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
cdef np.int32_t delta_function(np.int32_t i, np.int32_t j, np.int32_t k, np.int32_t m):
    if (i == k and j == m):
        return 1
    if (i == m and j == k):
        return 1
    return 0

class NeuralNetwork(Kernel):
    """
    Neural Network kernel with automatic relevance determination.
    This is a non-stationary kernel.

    Parameters
    ----------
    params : dict
        Dictionary of parameters for each type of kernel

    Notes
    -----
    Must specify the noise and function covariance, {'sigma_f', 'sigma_n'} as well
    as all sigma (not sigma squared) values for the :math:`\Sigma` matrix, which is symmetric:

    .. math::

        \Sigma = \begin{matrix}
                \sigma_{0,0}^2 & \sigma_{0,1}^2 & \dots & \sigma_{0,n}^2 \\
                \sigma_{1,0}^2 & \sigma_{1,1}^2 & \dots & \sigma_{1,n}^2 \\
                \dots & \dots & \dots & \dots & \\
                \sigma_{n,0}^2 & \sigma_{n,1}^2 & \dots & \sigma_{n,n}^2
            \end{bmatrix}

    where there are :math:`n` dimensions to the input data. :math:`\sigma_i` will
    correspond to the order of the input independent variables. Note than M is
    a (n+1) x (n+1) matrix and is symmetric, so only :math:`\sigma_{i,j}` where :math:`i \ge j`
    need to be specified.  If 2 entries are provided, for both (i,j) and (j,i), even
    if they are the same, an Exception will be thrown when attempting to perform
    calculations.  All sigma values which are not specified are assumed to be
    fixed at zero.

    Notes
    -----
    The kernel will operate on n-dimensional data even sigmas are only provided
    up to :math:`\sigma_k_k` where :math:`k < n`, however, if :math:`k > n` an
    Exception will be thrown.

    Only one :math:`\sigma_{i,j}` for a given (i,j) pair should be provided.

    Raises
    ------
    Exception : data provided with less dimensions than the covariances.

    Example
    -------
    >>> g = NeuralNetwork({'sigma_0_0':0.3, 'sigma_0_1':0.3, 'sigma_0_2':0.3, \
    ... 'sigma_1_1':0.4, 'sigma_1_2':0.5, 'sigma_2_2':0.6, 'sigma_f':1.0, 'sigma_n':0.001})
    """

    def __init__(self, **params):
        super(NeuralNetwork, self).__init__(**params)
        self.__distance_function = self.distance

    def kernel(self, x1, x2, oo, **kwargs):
        r"""
        Compute the Neural Network kernel. By default, assumes noiseless, unless oo=True.

        .. math::

            k(x_1, x_2) = \sigma_f^2 \left[ \frac{2}{\pi} {\rm sin}^{-1} \left( \frac{2\tilde{x_1}^T\Sigma\tilde{x_2}}{\sqrt{(1+2\tilde{x_1}^T\Sigma\tilde{x_2})(2\tilde{x_1}^T\Sigma\tilde{x_2})}} \right) \right] + \sigma_n^2 \delta(x_1, x_2)

        where :math:`\tilde{x}` is the augmented input vecor, :math:`(1, x_1, x_2, \dots, x_n)`.
        """
        if (oo):
            if (x1.shape != x2.shape):
                raise Exception('noise should be added to a square matrix')
            noise = np.eye(x1.shape[0], dtype=np.float32)*(self._params['sigma_n']**2)
        else:
            noise = 0.0

        return (self._params['sigma_f']**2)*2.0/np.pi*libc.math.asin(self.__distance_function(x1,x2)) + noise

    def partial_derivatives(self, x1, x2, oo=False, **kwargs):
        r"""
        Compute partial derivatives of kernel function with respect to all "sigma" values.
        Only sigma_i_j where j >= i are returned (upper triangular), since the sigma matrix is symmetric.

        Returns
        -------
        dict(str, ndarray)
            [partial sigma_0_0, partial sigma_0_1, ..., partial sigma_n_n, partial sigma_f, partial sigma_n]
        """

        ders = {}
        d, g = self.__distance_function(np.array(x1, dtype=np.float32), np.array(x2, dtype=np.float32), gradient=True)
        x = self._params['sigma_f']*2.0/np.pi*libc.math.asin(d)

        ders['sigma_f'] = 2*x
        for i in range(x1.shape[1]+1):
            for j in range(i, x1.shape[1]+1):
                ders['sigma_{}_{}'.format(i,j)] = (self._params['sigma_f']**2)*2.0/np.pi*(1.0/np.sqrt(1.0-d**2))*g[i,j]

        if (oo):
            if (x1.shape != x2.shape):
                raise Exception('noise should be added to a square matrix')
            sig_n_der = np.eye(x1.shape[0], dtype=np.float32)*(2*self._params['sigma_n'])
        else:
            sig_n_der = np.zeros((x1.shape[0],x2.shape[0]), dtype=np.float32)
        ders['sigma_n'] = sig_n_der

        return ders

    def _get_Sigma2(self):
        cdef int i, j, idx1, idx2, n

        use_keys = {}
        n = 0
        for k in self._params.keys():
            try:
                idx1, idx2 = int(k.split('_')[1]), int(k.split('_')[2])
            except:
                pass
            else:
                i,j = sorted([idx1,idx2])
                if (i,j) in use_keys:
                    raise Exception('duplicate specification of sigma_{}_{}'.format(i,j))
                use_keys[(i,j)] = k
                n = np.max([n,i,j])

        cdef np.ndarray[np.float32_t, ndim=2] Sigma2 = np.zeros((n+1,n+1), dtype=np.float32)
        for i,j in use_keys.keys():
            Sigma2[i,j] = self._params[use_keys[(i,j)]]**2
            Sigma2[j,i] = Sigma2[i,j]

        return Sigma2

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    def distance(self, np.ndarray[np.float32_t, ndim=2] x1,
        np.ndarray[np.float32_t, ndim=2] x2, bool gradient=False):

        cdef int i, j, k, m, p, q, delta, A = x1.shape[0], B = x2.shape[0], C = x2.shape[1]
        cdef np.float32_t dx_ox = 0.0, dx_oo = 0.0, dx_xx = 0.0, lo = 0.0, der = 0.0
        cdef np.float32_t dx_ox_k = 0.0, dx_oo_k = 0.0, dx_xx_k = 0.0
        cdef np.ndarray[np.float32_t, ndim=2] res = np.zeros((x1.shape[0], x2.shape[0]), dtype=np.float32)
        cdef np.ndarray[np.float32_t, ndim=4] grad = np.zeros((C+1, C+1, x1.shape[0], x2.shape[0]), dtype=np.float32)
        cdef np.ndarray[np.float32_t, ndim=2] Sigma2 = self._get_Sigma2()
        cdef np.ndarray[np.float32_t, ndim=2] Sigma = np.sqrt(Sigma2)
        cdef np.ndarray[np.float32_t, ndim=1] x1t = np.zeros(C+1, dtype=np.float32), x2t = np.zeros(C+1, dtype=np.float32)

        for i in range(A):
            x1t[0] = 1
            x1t[1:] = x1[i]
            for j in range(B):
                x2t[0] = 1
                x2t[1:] = x2[j]

                dx_ox = 0.0
                dx_oo = 0.0
                dx_xx = 0.0

                for k in range(C+1):
                    for m in range(C+1):
                        dx_ox += 2.0*x1t[k]*x2t[m]*Sigma2[k,m]
                        dx_oo += 2.0*x1t[k]*x1t[m]*Sigma2[k,m]
                        dx_xx += 2.0*x2t[k]*x2t[m]*Sigma2[k,m]

                lo = libc.math.sqrt((1.0+dx_oo)*(1.0+dx_xx))
                res[i,j] = dx_ox/lo

                if (gradient):
                    for p in range(C+1):
                        for q in range(p, C+1):
                            if (Sigma2[p,q] > 0.0):
                                dx_ox_k = 0.0
                                dx_oo_k = 0.0
                                dx_xx_k = 0.0
                                for k in range(C+1):
                                    for m in range(C+1):
                                        delta = delta_function(p,q,k,m)
                                        dx_ox_k += 4.0*Sigma[k,m]*x1t[k]*x2t[m]*delta
                                        dx_oo_k += 4.0*Sigma[k,m]*x1t[k]*x1t[m]*delta
                                        dx_xx_k += 4.0*Sigma[k,m]*x2t[k]*x2t[m]*delta

                                der = (1.0+dx_oo)*(dx_xx_k) + (1.0+dx_xx)*(dx_oo_k)
                                grad[p,q,i,j] = (lo*(dx_ox_k) - dx_ox*(0.5/lo*der))/(lo*lo)
                                grad[q,p,i,j] = grad[p,q,i,j]

        if (gradient):
            return res, grad
        else:
            return res

class GaussianARD(Kernel):
    """
    Gaussian kernel with automatic relevance determination for multiple dimensions.

    Parameters
    ----------
    params : dict
        Dictionary of parameters for each type of kernel

    Notes
    -----
    Must specify the noise and function covariance, {'sigma_f', 'sigma_n'} as well
    as all the diagonal values for the M matrix which determines the "relevance" of
    a dimension, {'sigma_1':1.234, 'sigma_2':2.345, ...}.

    .. math::

        M = \begin{matrix}
                \sigma_1^2 & 0 & \dots 0 \\
                0 & \sigma_2^2 & \dots & 0 \\
                \dots & \dots & \dots & \dots & \\
                0 & 0 & \dots & \sigma_n^2
            \end{bmatrix}

    where there are :math:`n` dimensions to the input data. :math:`\sigma_i` will
    correspond to the order of the input independent variables.

    Example
    -------
    >>> g = GaussianARD({'sigma_1':0.3, 'sigma_2':0.3, 'sigma_3':0.3, 'sigma_f':1.0, 'sigma_n':0.001})
    """

    def __init__(self, **params):
        super(GaussianARD, self).__init__(**params)
        self.__distance_function = self.euclidean

    def kernel(self, x1, x2, oo, **kwargs):
        r"""
        Compute the Gaussian kernel. By default, assumes noiseless, unless oo=True.

        .. math::

            k(x_1, x_2) = \sigma_f^2 {\rm exp} \left( - \frac{d(x_1,x_2) M d(x_1,x_2)^T}{2} \right) + \sigma_n^2 \delta(x_1, x_2)

        where :math:`M` is a square matrix of inverse lengths squared and :math:`d` denotes
        the distance between the two points.

        """
        if (oo):
            noise = np.eye(x1.shape[0])*(self._params['sigma_n']**2)
        else:
            noise = 0.0

        return (self._params['sigma_f']**2)*np.exp(-self.__distance_function(x1,x2)/2) + noise

    def partial_derivatives(self, x1, x2, oo=False, **kwargs):
        r"""
        Partial derivative of Gaussian kernel with respect to each of its parameters, sorted according to their name.

        .. math::

            \frac{\partial K}{\partial \sigma_f} = 2 \sigma_f {\rm exp} \left( - \frac{d(x_1,x_2) M d(x_1,x_2)^T}{2} \right)

            \frac{\partial K}{\partial \sigma_i = \sigma_f^2 {\rm exp} \left( - \frac{d(x_1,x_2) M d(x_1,x_2)^T}{2} \right) \frac{d(x_1,x_2)_i^2}{\sigma_i^3}

            \frac{\partial K}{\partial \sigma_n} = 2 \sigma_n \delta(x_1, x_2)

        Returns
        -------
        dict(str, ndarray)
            [partial sigma_1, partial sigma_2, ..., partial sigma_f, partial sigma_n]
        """

        ders = {}
        dMd, g = self.__distance_function(np.array(x1, dtype=np.float32), np.array(x2, dtype=np.float32), gradient=True)
        x = self._params['sigma_f']*np.exp(-dMd/2)

        ders['sigma_f'] = 2*x
        for i in range(x1.shape[1]):
            ders['sigma_{}'.format(i+1)] = self._params['sigma_f']*x*(-g[i])

        if (oo):
            if (x1.shape[0] != x2.shape[0]):
                raise Exception('noise should be added to a square matrix')
            sig_n_der = np.eye(x1.shape[0], dtype=np.float32)*(2*self._params['sigma_n'])
        else:
            sig_n_der = np.zeros((x1.shape[0],x2.shape[0]), dtype=np.float32)
        ders['sigma_n'] = sig_n_der

        return ders

    def _get_M(self):
        # Convert "sigma_1", "sigma_2", ... to matrix
        use_keys = {}
        for k in self._params.keys():
            try:
                idx = int(k.split('_')[1])
            except ValueError:
                pass
            else:
                use_keys[idx] = k
        M = np.eye(len(use_keys), dtype=np.float32)
        for i in range(M.shape[0]):
            M[i,i] = 1.0/self._params[use_keys[i+1]]**2

        return M

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def euclidean(self, np.ndarray[np.float32_t, ndim=2] x1, np.ndarray[np.float32_t, ndim=2] x2, bool gradient=False):
        cdef int i, j, k, A = x1.shape[0], B = x2.shape[0], C = x2.shape[1]
        cdef np.float32_t dx = 0.0
        cdef np.ndarray[np.float32_t, ndim=2] res = np.zeros((x1.shape[0], x2.shape[0]), dtype=np.float32)
        cdef np.ndarray[np.float32_t, ndim=3] grad = np.zeros((x2.shape[1], x1.shape[0], x2.shape[0]), dtype=np.float32)
        cdef np.ndarray[np.float32_t, ndim=2] M = self._get_M()
        cdef np.ndarray[np.float32_t, ndim=2] Mder = M**(3/2.)

        for i in range(A):
            for j in range(B):
                for k in range(C):
                    dx = x1[i,k] - x2[j,k]
                    res[i,j] += dx*M[k,k]*dx
                    if (gradient):
                        grad[k,i,j] = dx**2*(-2*Mder[k,k])

        if (gradient):
            return res, grad
        else:
            return res

class Gaussian(Kernel):
    """
    Gaussian kernel.  The distance function can be specified according to the type.

    Parameters
    ----------
    params : dict
        Dictionary of parameters for each type of kernel

    Notes
    -----
    Valid types include:
    * 'Euclidean' (default) : {'sigma_l', 'sigma_f', 'sigma_n'}

    Example
    -------
    >>> g = Gaussian({'sigma_l':0.3, 'sigma_f':1.0, 'sigma_n':0.001, 'type':'Euclidean'})
    """

    def __init__(self, **params):
        super(Gaussian, self).__init__(**params)

        # Default to Euclidean distance
        self.__distance_function = self.euclidean_distance_squared

        # Otherwise select type
        try:
            distance_type = self._params['type']
            if (distance_type == 'orthodromic'):
                raise Exception('not implemented')
        except KeyError:
            pass

    def kernel(self, x1, x2, oo, **kwargs):
        r"""
        Compute the Gaussian kernel. By default, assumes noiseless, unless oo=True.

        .. math::

            k(x_1, x_2) = \sigma_f^2 {\rm exp} \left( - \frac{d(x_1,x_2)}{2 \sigma_l^2} \right) + \sigma_n^2 \delta(x_1, x_2)

        where :math:`d` denotes the distance (squared) between the two points.

        """

        if (oo):
            noise = np.eye(x1.shape[0], dtype=np.float32)*(self._params['sigma_n']**2)
        else:
            noise = 0.0

        return (self._params['sigma_f']**2)*np.exp(-self.__distance_function(x1,x2)/(2*self._params['sigma_l']**2)) + noise

    def partial_derivatives(self, x1, x2, oo=False, **kwargs):
        r"""
        Partial derivative of Gaussian kernel with respect to each of its parameters, sorted according to their name.

        .. math::

            \frac{\partial K}{\partial \sigma_f} = 2 \sigma_f {\rm exp} \left( - \frac{d(x_1,x_2)}{2 \sigma_l^2} \right)

            \frac{\partial K}{\partial \sigma_l} = \sigma_f^2 {\rm exp} \left( - \frac{d(x_1,x_2)}{2 \sigma_l^2} \right) \frac{d(x_1,x_2)}{\sigma_l^3}

            \frac{\partial K}{\partial \sigma_n} = 2 \sigma_n \delta(x_1, x_2)

        Returns
        -------
        dict(str, ndarray)
            [partial sigma_f, partial sigma_l, partial sigma_n]
        """

        d = self.__distance_function(x1,x2)
        x = self._params['sigma_f']*np.exp(-d/(2*self._params['sigma_l']**2))

        sig_f_der = 2*x
        sig_l_der = self._params['sigma_f']*x/(self._params['sigma_l']**3)*d

        if (oo):
            if (x1.shape[0] != x2.shape[0]):
                raise Exception('noise should be added to a square matrix')
            sig_n_der = np.eye(x1.shape[0], dtype=np.float32)*(2*self._params['sigma_n'])
        else:
            sig_n_der = np.zeros((x1.shape[0],x2.shape[0]), dtype=np.float32)

        return {'sigma_f':sig_f_der, 'sigma_l':sig_l_der, 'sigma_n':sig_n_der}

    @staticmethod
    def euclidean_distance_squared(x1, x2):
        return np.sum(x1**2,1).reshape(-1,1) + np.sum(x2**2,1) - 2*np.dot(x1, x2.T)
