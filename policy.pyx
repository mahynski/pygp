import scipy.optimize
import warnings
import gaussian_process
import numpy as np
import cython

cimport cython
cimport libc.math
cimport numpy as np

class Policy(object):
    def __init__(self, **params):
        self._params = params

class UpperConfidenceBound(Policy):
    r"""
    Find coordinate at maximum upper confidence bound.

    .. math::

        {\rm argmax} \left( \mu + \sqrt{\beta(t)}\sigma \right)

    where :math:`\mu` is the predicted mean and :math:`\sigma` is the predicted standard deviation.
    Note that :math:`\beta` is a function of "time", or step in the process.  This time, t, is inferred from
    the number of points found in a GaussianProcess' history at the time UpperConfidenceBound.get() is called.

    Parameters
    ----------
    delta : float
        Determines the upper confidence bound according to Theorem 1 of Srinivas et al., (2010).
        Should be between 0 and 1.
    tol_factor : float
        Bounds in each dimension are explored up to (max-min)*tol_factor.  This is to prevent matrices from
        failing to be postitive definite.  These bounds may be overridden by manually specifying
        them as a keyword argument in optimize_kwargs.
    D : float
        Net (hyper)volume of domain.  If None, this is inferred from the GaussianProcess' prior later.
        Otherwise, this may be manually specified ahead of time here.
    optimize_kwargs : dict
        Dictionary of keyword arguments for scipy.optimize.minimize.
    """
    def __init__(self, delta=0.5, tol_factor=1.0e-9, D=None, **optimize_kwargs):
        super(UpperConfidenceBound, self).__init__(**optimize_kwargs)
        self._params['method'] = 'L-BFGS-B'
        self.__tol_factor = tol_factor
        self.__delta = delta
        self.__D = D

    @staticmethod
    def sqrt_beta_t(t, delta, D):
        return np.sqrt(2*np.log(D*(t**2)*np.pi**2/6.0/delta))

    def get(self, gp):
        """
        Compute the next point to sample from a given GaussianProcess by searching for the
        global maximum in the upper confidence bound.

        Parameters
        ----------
        gp : GaussianProcess
            Gaussian process to consider.

        Returns
        -------
        OptimizeResult
            Result of optimization.  User should check if OptimizeResult.success is True.
            OptimizeResult.x is the UCB coordinate.

        Notes
        -----
        The initial guess for this optimization is given by looking over the coordinates of the prior.
        From there, the gradient-based L-BFGS-B (with numerical Jacobian) optimizer is used by default.
        Unless otherwise specified, bounds are imposed automatically at the limits of the original x_prior
        within some tolerance.
        """

        if (self.__D is None):
            # Assume D must be computed from x_prior
            bound_vector = []
            for k in range(gp.x_prior.shape[1]):
                lb, ub = np.min(gp.x_prior[:,k]), np.max(gp.x_prior[:,k])
                bound_vector.append([lb, ub, ub-lb])
            bound_vector = np.array(bound_vector)
            D = np.prod(bound_vector[:,2])
        else:
            # Use a specified domain value instead
            D = self.__D

        upper_confidence_bound = lambda x: x[0]+self.sqrt_beta_t(gp.history['x'].shape[0], self.__delta, D)*x[1]

        def compute(new_x):
            mean, stdev = gp.predict(new_x)
            return -upper_confidence_bound((mean,stdev))

        eps = np.sqrt(np.finfo(float).eps)
        def jacobian(new_x):
            return scipy.optimize.approx_fprime(new_x, compute, eps)

        self._params['jac'] = jacobian
        if ('bounds' not in self._params):
            min_v = [np.min(gp.x_prior[:,i]) for i in range(gp.x_prior.shape[1])]
            max_v = [np.max(gp.x_prior[:,i]) for i in range(gp.x_prior.shape[1])]
            tol = [(max_v[i] - min_v[i])*self.__tol_factor for i in range(gp.x_prior.shape[1])]
            self._params['bounds'] = [(min_v[i]+tol[i], max_v[i]-tol[i])
                                         for i in range(gp.x_prior.shape[1])]

        # Initially guess at maximum from the prior, otherwise start in the "middle"
        try:
            mean, stdev, f_post = gp.posterior(x_observed=gp.history['x'],
                                                 y_observed=gp.history['y'],
                                                 n=0, save=False, use_history=False)
            bounds = upper_confidence_bound((mean,stdev))
            init_guess = gp.x_prior[np.where(bounds == np.max(bounds))[0][0]]
        except:
            init_guess = np.mean(gp.x_prior, axis=0)

        res = scipy.optimize.minimize(compute, init_guess, **self._params)

        return res
