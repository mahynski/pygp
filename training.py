import gaussian_process
import kernel
import policy
import numpy as np
import copy
import traceback
from sklearn.model_selection import KFold

def ucb_training(gp,
                 x_bounds,
                 latent_function,
                 n_warm_ups=3,
                 delta=0.5,
                 budget_per_epoch=1,
                 n_epochs=1,
                 seed=1234,
                 optimize_kwargs={},
                 k_split=5,
                 tol_factor=1.0e-6):
    """
    Train a gaussian process over a finite domain. After each epoch, the hyperparameters are optimized
    based on log marginal likelihood.  In addition, k-fold cross-validation (CV) is performed on these optimized
    results to monitor the progress of the training; however, optimization is NOT done with respect to the CV
    score.

    Parameters
    ----------
    gp : GaussianProcess
        Gaussian process to do UCB training on.
    x_bounds : dict
        Dictionary of (min, max) bounds for each dimension of the data. This defines the domain, D.
        These will be sorted by name (alphabetical, etc.) and this order is assumed to be what
        ``latent_function`` is anticipating.
    latent_function : callable
        Function that accepts an array of x values (in name-sorted order according to x_bounds) and returns a scalar.
    n_warm_ups : int
        Number of random points to sample.
    delta : float
        Determines the upper confidence bound according to Theorem 1 of Srinivas et al., (2010)
    budget_per_epoch : int
        Number of samples to query during an epoch.
    n_epochs : int
        Number of epochs to perform.
    seed : int
        Sets the random number generator for reproducibility.
    optimize_kwargs : dict
        Keyword arguments for GaussianProcess.optimize_kernel(). Default method applied is SLSQP.
    k_split : int
        Number of disjoint sets to break results into to perform cross-validation.  Best practices are to use
        k_split = 5, or k_split = 10.
    tol_factor : float
        Bounds are explored up to (max-min)*tol_factor.  This is to prevent matrices from
        failing to be postitive definite.  These bounds may be overridden by manually specifying
        them as a keyword argument in optimize_kwargs.

    Notes
    -----
    A GP may be trained with some data, or given some prior, before performing UCB training.  If this is the
    case, a random "warm up" stage may not be necessary.

    Warning
    -------
    The ``latent_function`` must accept input as a numpy array of values in the sorted order of the names
    given in ``x_bounds``. This sorted order is established internally and used.

    Returns
    -------
    tuple(list(list), list(dict), dict(str, list(float, float)))
        Finally, the results_log, param_log, cv_log are returned.
        results_log: [epoch, count, mu, std, res] for each epoch
        param_log: optimized parameters for kernel
        cv_log: k-fold cross-validation (mean, standard deviation) using log pseudo-likelihood for current set of optimized parameters and the last (valid) set.
    """

    # Set random number generator seed (state) so as to be reproducible
    training_state = np.random.RandomState(seed)

    # Get domain size
    bound_vector = []
    for k in sorted(x_bounds.keys()):
        lb, ub = np.min(x_bounds[k]), np.max(x_bounds[k])
        bound_vector.append([lb, ub, ub-lb])
    bound_vector = np.array(bound_vector)
    print 'Assuming that bounds order of : {}'.format(sorted(x_bounds.keys()))

    # Confidence bound multiplier
    D = np.prod(bound_vector[:,2])
    d = bound_vector.shape[0] # dimensionality of the data

    ucb_policy = policy.UpperConfidenceBound(tol_factor=tol_factor, delta=delta, D=D)
    #,**{'options':{'ftol':1.0e-3, 'gtol':1.0e-3, 'maxiter':100}})

    # Warm up the GP with some random observations, if needed
    if (n_warm_ups > 0):
        x_obs = training_state.rand(n_warm_ups*d).reshape(n_warm_ups,d)*bound_vector[:,2] + bound_vector[:,0]
        y_obs = np.array([latent_function(x) for x in x_obs], dtype=np.float32).reshape(n_warm_ups,1)
        gp.posterior(x_observed=x_obs, y_observed=y_obs, n=0, save=True, use_history=True)

    res = ucb_policy.get(gp)
    success = res.success

    results_log = []
    param_log = []
    cv_log = []

    kernel_clone = copy.deepcopy(gp.kernel)
    param_log.append(copy.copy(kernel_clone.parameters)) # Initially store parameters for loop below

    batch_size = 1
    for epoch in range(n_epochs):
        for count in range(budget_per_epoch):
            last_res = res
            if (success):
                # Use upper confidence bound to decide next state point
                x_obs = res.x.reshape(batch_size,d)
            else:
                # Make random choice(s)
                x_obs = training_state.rand(batch_size*d).reshape(batch_size,d)*bound_vector[:,2] + bound_vector[:,0]
            y_obs = np.array([latent_function(x) for x in x_obs], dtype=np.float32).reshape(batch_size,1)

            try:
                mu, std, post = gp.posterior(x_observed=x_obs, y_observed=y_obs, n=0, save=True, use_history=True)
                results_log.append([epoch, count, mu, std, last_res])
            except Exception as e:
                warnings.warn('Trying random choice instead : {}'.format(e))
                success = False
            else:
                try:
                    res = ucb_policy.get(gp)
                    success = res.success
                except:
                    success = False

            # Pause as an iterator until told to move on - param_log[0] had initial params in it
            yield (results_log, param_log[1:], cv_log)

        # 1. Optimization to best describe all the data we have at the moment
        if ('method' not in optimize_kwargs):
            optimize_kwargs['method'] = 'SLSQP'

        try:
            opt_params, opt_res = gp.optimize_kernel(**optimize_kwargs)
        except:
            # In the event of unknown errors, use the last set of parameters
            print 'Failed to optimize kernel : {}'.format(traceback.format_exc())
            opt_params = param_log[-1]
        else:
            if (opt_res.success):
                # If optimization converged, use new parameters
                gp.update_kernel_parameters(**opt_params)
            else:
                # Otherwise, use last set of parameters
                opt_params = param_log[-1]

        param_log.append(opt_params)

        # 2. Cross-validation consistency check
        try:
            cv_res = {}
            # Compare last set and current set of parameters on current data
            for params,round in zip(param_log[-2:], ['last','current']):
                kernel_clone.update_parameters(**params)
                kf = KFold(n_splits=k_split, shuffle=True, random_state=training_state)
                log_pseudo_likelihood_score = []
                for train_index, test_index in kf.split(np.arange(gp.history['x'].shape[0])):
                    try:
                        train_x, train_y = gp.history['x'][train_index], gp.history['y'][train_index]
                        test_x, test_y = gp.history['x'][test_index], gp.history['y'][test_index]
                        log_pseudo_likelihood_score.append(gaussian_process.log_marginal_likelihood(train_x, kernel_clone, train_y)[0])
                    except:
                        # If there is an error with a specific subset, still try others
                        pass
                cv_res[round] = [np.mean(log_pseudo_likelihood_score), np.std(log_pseudo_likelihood_score)]
            cv_log.append(cv_res)
        except Exception as e:
            cv_log.append({'last':[None, None], 'current':[None, None], 'error':str(e)})

    yield (results_log, param_log[1:], cv_log)
