import copy
import warnings
import numpy as np
import scipy.optimize

np.seterr(under='print') # Suppress underflow warnings

is_positive_definite = lambda x: np.all(np.linalg.eigvals(x) > 0)

def safe_cholesky(x, delta=1.0e-6, max_iters=10):
    """
    Adds a stabilizing amount of noise to the diagonals of the matrix to allow Cholesky decomposition.
    If the Cholesky decomposition cannot be achieved without stabilization, :math:`10*\delta^{i-1}` is added
    to the diagonal for iteration, :math:`i`.

    Parameters
    ----------
    x : ndarray
        Square matrix to perform Cholesky decomposition on.
    delta : float
        Differential amount to (start) adding to the diagonal to stabilize the matrix decomposition.
    max_iters : int
        Number of times to increase delta in an attempt to stabilize the decomposition.

    Returns
    -------
    ndarray
        Lower triangular matrix L where x = LL^T

    Raises
    ------
    Exception : if unable to stabilize the Cholesky decoposition in the given number of max iterations.
    """

    iters = 0
    stabilizer = 0
    while(iters < max_iters):
        try:
            L = np.linalg.cholesky(x + np.eye(x.shape[0], dtype=np.float64)*stabilizer)
        except np.linalg.LinAlgError:
            if (iters == 0):
                stabilizer = delta
            else:
                stabilizer *= 10
        else:
            return L
        iters += 1

    raise Exception('unable to stabilize cholesky decomposition after {} iterations : min eigenvalue = {}'.format(max_iters,np.min(np.linalg.eigvals(x + np.eye(x.shape[0], dtype=np.float64)*stabilizer))))

def log_marginal_likelihood(x, kernel, y, min_det=np.finfo(float).eps, max_iters=10):
    """
    Compute log marginal likelihood and its gradient.

    Parameters
    ----------
    x : ndarray(float)
        Coordinates investigated
    kernel : Kernel
        Kernel function
    y : ndarray(float)
        Observations at x
    min_det : float
        If determinant of kernel drops below (the absolute value of this) number, it is reset to this.
        This can sometimes occur during optimization so this can sometimes be a useful tool; other
        times it can impede/confuse the optimization.
    max_iters : int
        Number of times to increase the Cholesky stabilizer by a factor of 10.

    Returns
    -------
    (float, ndarray)
        Function, partial derivative with respect to kernel's parameters in sorted order
    """

    K = kernel(x, x, oo=True)
    init_K = copy.copy(K)

    # Attempt to stabilize
    iters = 0
    stabilizer = 0
    while (not is_positive_definite(K) and iters < max_iters):
        K = init_K + np.eye(K.shape[0])*stabilizer
        if (stabilizer == 0):
            stabilizer = 1.0e-6
        else:
            stabilizer *= 10
        iters += 1

    if (iters == max_iters):
        raise Exception('K is not positive definite in log_marginal_likelihood')

    # Bound domain of logarithm and numerically protect
    det = np.abs(np.linalg.det(K))
    if (det < np.abs(min_det)):
        det = np.abs(min_det)

    # If positive definite, this can be optimized
    K_inv = np.linalg.inv(K)

    f = -0.5*np.dot(np.dot(y.T, K_inv), y) \
        -0.5*np.log(det) \
        -0.5*x.shape[0]*np.log(2*np.pi)

    # g can be further optimized -- see RW Ch 5
    alpha = np.dot(K_inv, y)
    part_der = kernel.partial_derivatives(x, x, oo=True)
    g = np.array([0.5*np.trace(np.dot(np.outer(alpha, alpha.T) - K_inv, part_der[k])) for k in sorted(part_der.keys())])

    return f,g

class GaussianProcess(object):
    """
    Class for a generic Gaussian Process.

    Parameters
    ----------
    x_prior : ndarray
        (N,m) array of N points with m dimensions defining the parameter space.
    kernel : Kernel
        Kernel function.  An internal copy is made.
    seed : int
        Seed for numpy.random.RandomState
    """
    def __init__(self, x_prior, kernel=None, seed=123456789):
        self.__x_prior = copy.deepcopy(np.array(x_prior, dtype=np.float32))
        self.__kernel = copy.deepcopy(kernel)
        self.__state = np.random.RandomState(seed)

        self.__observed = None
        self.__K_ss = np.array(self.__kernel(self.__x_prior, self.__x_prior, oo=False), dtype=np.float32)

    def prior(self, n=10):
        """
        Sample a number of functions from the prior.

        Parameters
        ----------
        n : int
            Number of functions to draw from the prior
        """

        L = safe_cholesky(self.__K_ss)
        f_prior = np.dot(L, self.__state.normal(size=(self.__x_prior.shape[0],n)))

        return f_prior

    @property
    def history(self):
        """
        Return the saved history of the process.

        Returns
        -------
        dict
            Dictionary of "x" and "y"
        """
        return self.__observed

    @property
    def x_prior(self):
        return self.__x_prior

    @property
    def kernel(self):
        return self.__kernel

    def predict(self, new_x):
        """
        Make a prediction based on known history of observations.
        This only uses data stored in the GaussianProcess' history; see
        posterior() to add new observations.

        Parameters
        ----------
        new_x : ndarray
            Array of row vectors of new coordinate(s) to make predictions at.

        Returns
        -------
        ndarray, ndarray
            mean, stdev at each point
        """

        xp = np.array(new_x.reshape(-1, self.__x_prior.shape[1]), dtype=np.float32)
        mean, stdev, L_k = self._compute(xp, self.__observed['x'], self.__observed['y'], \
            self.__kernel(xp, xp, oo=False))

        return mean, stdev

    def _compute(self, xp, xo, yo, Kss):
        """
        Perform gaussian process computations for other internal functions.
        This is not intended to be interacted with directly.

        Parameters
        ----------
        xp : ndarray
            Array of points as rows to make predictions at (should correspond to K**).
        xo : ndarray
            Coordinates (in row format) that have been observed (corresponds to K).
        yo : ndarray
            Scalar observations (in row format) from xo.
        Kss : ndarray
            K** matrix from kernel calculation involving the points to predict at.

        Returns
        -------
        ndarray, ndarray, ndarray
            mean, stdev, L_k
        """

        # This method follows from Appendix A.4 of RW but requires stabilization of the Cholesky decomposition to actually work.
        K = self.__kernel(xo, xo, oo=True)
        if (not is_positive_definite(K)):
            warnings.warn('K is not positive definite, will attempt to stabilize it')

        L = safe_cholesky(K)
        K_s = self.__kernel(xo, xp, oo=False)
        L_k = np.linalg.solve(L, K_s)
        mean = np.dot(L_k.T, np.linalg.solve(L, yo)).reshape((xp.shape[0]),)
        stdev = np.sqrt(np.abs(np.diag(Kss) - np.sum(L_k**2, axis=0))) # Absolute value to protect sqrt() next

        return mean, stdev, L_k

    def posterior(self, x_observed, y_observed, n=0, save=False, use_history=False):
        """
        Compute the posterior given a set of observations; i.e., predict the value
        at all x_prior coordinates given these observations, potentially including
        previous observations as well.

        Parameters
        ----------
        x_observed : ndarray
            Coordinates of observation
        y_observed : ndarray
            Observation values
        n : int
            Number of functions to draw from the posterior
        save : bool
            Whether or not to save these observations and continue to use them in the future
        use_history : bool
            Whether or not to use the full history of observations in addition to current ones

        Returns
        -------
        tuple(ndarray, ndarray, ndarray)
            mean, stanard deviation of new observations and posterior function values
        """

        def save_data(x, y):
            if (not self.__observed):
                self.__observed = {'x':x, 'y':y}
            else:
                self.__observed['x'] = np.concatenate((self.__observed['x'], x), axis=0)
                self.__observed['y'] = np.concatenate((self.__observed['y'], y), axis=0)

        if (use_history and self.__observed):
            x_obs = np.concatenate((self.__observed['x'], x_observed.reshape(-1,self.__x_prior.shape[1])), axis=0)
            y_obs = np.concatenate((self.__observed['y'], y_observed.reshape(-1,1)), axis=0)
        else:
            x_obs = x_observed.reshape(-1,self.__x_prior.shape[1])
            y_obs = y_observed.reshape(-1,1)

        x_obs = np.array(x_obs, dtype=np.float32)
        y_obs = np.array(y_obs, dtype=np.float32)

        try:
            mean, stdev, L_k = self._compute(self.__x_prior, x_obs, y_obs, self.__K_ss)
            if (n > 0):
                L = safe_cholesky(self.__K_ss - np.dot(L_k.T, L_k))
                f_post = mean.reshape(-1, 1) + \
                        np.dot(L, self.__state.normal(size=(self.__x_prior.shape[0],n)))
            else:
                f_post = None
        except Exception as e:
            raise Exception('unable to compute posterior : {}'.format(e))
        else:
            if (save):
                save_data(np.array(x_observed, dtype=np.float32).reshape(-1,self.__x_prior.shape[1]),
                          np.array(y_observed, dtype=np.float32).reshape(-1,1))

            return (mean, stdev, f_post)

    def update_kernel_parameters(self, **new_params):
        """
        Update the processes' kernel parameters and recomputes K**.
        It is important that the latter be done, so **do not** simply update
        the kernel parameters manually.

        Parameters
        ----------
        new_params : dict(str, float)
            Dictionary of kernel parameters.
        """

        self.__kernel.update_parameters(**new_params)
        self.__K_ss = np.array(self.__kernel(self.__x_prior, self.__x_prior, oo=False), dtype=np.float32)

    def optimize_kernel(self, numerical_jacobian=False, **optimize_kwargs):
        """
        Find the optimal kernel hyperparameters for the currently observed set of data in the processes' history
        using marginal likelihood. Optimization begins from the kernel's parameter values when this
        method is invoked. Alternative cross-validation approaches are also possible but are not
        implemented in this method. A copy of the kernel is optimized, **not** the current GaussianProcess' kernel.
        This must be updated with the new parameters, if so desired.

        Parameters
        ----------
        numerical_jacobian : bool
            If True, the Jacobian is numerically approximated for optimizers that use it.
        optimize_kwargs : dict
            Dictionary of keyword arguments for scipy.optimize.minimize. Note that if bounds are specified,
            they should be provided as a dictionary based on their name since the order will be determined
            internally.

        Returns
        -------
        dict(str, float), OptimizeResult
            Dictionary of optimized parameter values, result returned by optimizer.

        Notes
        -----
        The user should check OptimizeResult.success to see if the optimization was successful.
        By default, SLSQP is used for optimization.
        For any optimizer capable of using the Jacobian, it is enabled and computed numerically.

        If there are no points measured and recorded in the history, this will fail.
        """

        kernel = copy.deepcopy(self.__kernel)

        # Put parameters in a sorted order to be consistent with log_marginal_likelihood derivatives
        param_names = sorted(kernel._params.keys())
        param_vector = [kernel._params[k] for k in param_names]
        if ('bounds' in optimize_kwargs):
            optimize_kwargs['bounds'] = [optimize_kwargs['bounds'][name] for name in param_names]

        # Minimize the negative likelihood to maximize the positive likelihood
        def objective(param_vector, kern, pnames, x, y):
            kern.update_parameters(**dict(zip(pnames, param_vector.reshape(len(pnames),))))
            try:
                f, g = log_marginal_likelihood(x, kern, y)
            except Exception:
                # Numerical protection against inability to stabilize Cholesky
                f,g = -np.inf, -np.inf

            if (not numerical_jacobian):
                return -f, -g
            else:
                return -f

        # Numerical approach
        def num_jacobian(param_vector, kern, pnames, x, y):
            return scipy.optimize.approx_fprime(param_vector,
                                                objective,
                                                np.sqrt(np.finfo(float).eps),
                                                kern, pnames, x, y)

        # Default optimization method
        if ('method' not in optimize_kwargs):
            optimize_kwargs['method'] = 'SLSQP'

        # Enable Jacobian
        if (optimize_kwargs['method'] in ['CG', 'BFGS', 'Newton-CG', 'L-BFGS-B', 'TNC', 'SLSQP', 'dogleg', 'trust-ncg',
                                         'trust-krylov', 'trust-exact', 'trust-constr']):
            if (numerical_jacobian):
                optimize_kwargs['jac'] = num_jacobian
            else:
                optimize_kwargs['jac'] = True

        res = scipy.optimize.minimize(objective, param_vector,
                                      args=(kernel, param_names, self.__observed['x'], self.__observed['y']),
                                      **optimize_kwargs)

        return dict(zip(param_names, res.x)), res
