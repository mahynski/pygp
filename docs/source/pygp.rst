pygp package
============

.. automodule:: pygp
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

pygp\.gaussian\_process module
------------------------------

.. automodule:: pygp.gaussian_process
    :members:
    :undoc-members:
    :show-inheritance:

pygp\.kernel module
-------------------

.. automodule:: pygp.kernel
    :members:
    :undoc-members:
    :show-inheritance:

pygp\.policy module
-------------------

.. automodule:: pygp.policy
    :members:
    :undoc-members:
    :show-inheritance:

pygp\.training module
---------------------

.. automodule:: pygp.training
    :members:
    :undoc-members:
    :show-inheritance:


